package idealista.java.gof.strategy;

import idealista.java.transform.Ad;
import idealista.java.transform.User;
import sun.plugin2.message.Message;

import java.time.LocalDate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaStyle {

        static String render(Ad ad, Predicate<Ad.Typology> typologyChecker, Function<Ad, String> titleCreator) {
        if(typologyChecker.test(ad.getTypology()))
            return titleCreator.apply(ad);
        return "";
    }

    public static void main(String[] args) {
        Ad ad = new Ad(new User("ans", "9256983"), LocalDate.now(), Ad.Typology.Office, Ad.Operation.Rent, 26d);
        Predicate<Ad.Typology> isAHouse= typology -> typology == Ad.Typology.House;
        Predicate<Ad.Typology> isAnOffice= typology -> typology == Ad.Typology.Office;
        Function<Ad, String> titleAsOffice = targetAd -> "Esta Oficina molona cuesta " + targetAd.getPrice();
        System.out.println(render(ad,
                isAHouse,
                titleAsOffice));
    }


}
